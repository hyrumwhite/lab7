var express = require('express'),
    routes = require('./routes/routes'),
    cors = require('cors');
    var lab7 = require("./routes/lab7");

var app = express();
app.use(express.static('./public'));
app.configure(function() {
  app.use(express.bodyParser());
  app.use(app.router);
});
app.get("/cities", cors(), lab7.filter);
app.options("/cities", cors());
app.listen(3000);
console.log('Listening on port 3000...');